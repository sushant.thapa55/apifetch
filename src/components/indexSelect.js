import React, { useEffect, useState } from 'react'
import './select.css'
function IndexSelect() {


   const FetchApi = () => {
       fetch("http://localhost:3001/api/spreadsheet")
       .then((response) => response.json())
       .then((json) =>{
        //    console.log(json)
           console.log(json.values)
       });
   };




    const countries = [
        {id:"1",name:"Nepal"},
        {id:"2",name:"USA"}

    ];


    const states =[
        {id:"1",countryId:"1",name:"Province 2"},
        {id:"2",countryId:"1",name:"Province 3"},
        {id:"3",countryId:"2",name:"New York"},
        {id:"4",countryId:"2",name:"Texas"}
    ];

    const cities = [
        { id:"1", stateId:"1",name:"Pokhara"},
        { id:"2", stateId:"1",name:"Mustang"},
        { id:"3", stateId:"2",name:"Kathmandu"},
        { id:"4", stateId:"2",name:"Kavre"},
        { id:"5", stateId:"3",name:"State 1"},
        { id:"6", stateId:"3",name:"State 2"},
        { id:"7", stateId:"4",name:"State 3"},
        { id:"8", stateId:"4",name:"State 4"}
    ];

    const districts = [
        {id:"1",citieId:"1",name:"LakeSide"},
        {id:"2",citieId:"1",name:"Mahendra Cave"},
        {id:"3",citieId:"1",name:"Binda Basini"},
        {id:"4",citieId:"2",name:"Pokhara Airport"},
        {id:"5",citieId:"2",name:"Pokhara Rangasala"},
        {id:"6",citieId:"3",name:"pokhara University"},
    ];



    const[country,setCountry] = useState([]);
    const[state,setState] = useState([]);
    const[city,setCity] = useState([]);
    const[district,setDistrict] = useState([]);



    


    useEffect(() =>
    {
        setCountry(countries);
        FetchApi();
        
    },[])


    const handleCountry = (id) => {
        const dt = states.filter(x =>x.countryId ===id);
        setState(dt);
    }

    const handleState = (id) => {
        const dt = cities.filter(x => x.stateId === id);
        setCity(dt);
    }

    const handleDistrict = (id) => {
        const dt = districts.filter(x => x.citieId === id);
        setDistrict(dt);
    }
   


  return (
    <div>
        <center>        
        <div className='main-container'>
            <div className='first-row'>
            <select id='first-selector' onChange={(e) => handleCountry(e.target.value)} >
        <option value='0'>-Select Option-</option>
        {
            country &&
            country !== undefined ?
            country.map((ctr,index) => {
                return (
                    <option key={index} value={ctr.id}>{ctr.name}</option>
                )
            })
            :"No Country"
        }    
    </select>
        <select id='second-selector' onChange={(e) => handleState(e.target.value)}> 
            <option>-Select Option-</option>
        {
            state &&
            state !== undefined ?
            state.map((ctr,index) => {
                return(
                    <option key={index} value={ctr.id} >{ctr.name}</option>
                )
            })
            :"No State"

        }

        </select>

            </div >

        <div className='second-row'>
            <select id='third-selector'  onChange={(e) => handleDistrict(e.target.value)}> 
            <option>-Select Option-</option>
            {
                city &&
                city !== undefined ?
                city.map((ctr,index) =>{
                    return (
                        <option key={index} value={ctr.id}>{ctr.name}</option>
                    )

                })
                :"No City"
            }

        </select>
        <select id='fourth'> 
            <option>-Select Option-</option>
            
        {
            district &&
            district !== undefined ?
            district.map((ctr,index) =>{
                return (
                    <option key={index} value={ctr.id} >{ctr.name}</option>
                )
            })
            :"No District"
        }
        
        
        </select>

            </div>


        </div>
        </center>


    </div>
  )
}

export default IndexSelect
import React, { useEffect, useState } from 'react'

function ApiFetch() {
  
    const [list,setList] = useState([""]);

    const getApi = ()=> {
        fetch("https://jsonplaceholder.typicode.com/posts")
        .then((response) => response.json())
        .then((json) =>{
           
            setList(json);
            console.log(json);
        
        }); 
    };


    useEffect(() =>{
        getApi();
    },[]);

  
  
    return (
    <>

<table class="table">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">Title</th>
      <th scope="col">Body</th>
      
    </tr>
  </thead>
  <tbody>
            {
                list.map((item)=>
                {
               return  <tr>
                        <td>{item.id}</td>
                        <td>{item.title}</td>
                         <td>{item.body}</td>
                        
                    </tr>
                })
            }
   
  </tbody>
</table>


    
    
    </>




  )
}

export default ApiFetch